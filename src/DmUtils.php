<?php 
namespace dmutils;  //命名空间遵循psr-4规范

use dmutils\utils\Test;
use dmutils\utils\Random;

class DmUtils{

    public static function test($name="da ming"){
        return Test::test($name);
    }
    public static function random($name="da ming"){
        return Random::random($name);
    }    
}